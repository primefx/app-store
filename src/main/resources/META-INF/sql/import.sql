INSERT into Role values (1, 'ROLE_ADMIN');
INSERT into Role values (2, 'ROLE_USER');

INSERT into User values (1, true, 'admin', 'admin');
INSERT into User values (2, true, 'user', 'user');

INSERT into User_Role values (1,1);
INSERT into User_Role values (1,2);
INSERT into User_Role values (2,2);