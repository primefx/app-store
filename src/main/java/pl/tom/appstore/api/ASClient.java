package pl.tom.appstore.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Cacheable(value = { "appStore" })
public class ASClient {

	private final static String SEARCH_URL = "https://itunes.apple.com/search?term={term}&media={media}";
	private final static String LOOKUP_URL = "https://itunes.apple.com/lookup?id={id}";
	private final static String DEFAULT_MEDIA = "software";

	private final Logger log = LoggerFactory.getLogger(ASClient.class);

	@Autowired
	private RestTemplate restTemplate;

	public ASResponse get(String term) {
		return get(term, DEFAULT_MEDIA);
	}

	public ASResponse get(ASRequest request) {

		log.debug("AppStore get: {}", request);

		ASResponse response = null;
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			StringBuilder sb = new StringBuilder(SEARCH_URL);

			map.put("term", request.getTerm());
			map.put("media", request.getMedia());

			if (StringUtils.isNotBlank(request.getCountry())) {
				map.put("country", request.getCountry());
				sb.append("&").append("country={country}");
			}

			if (StringUtils.isNotBlank(request.getEntity())) {
				map.put("entity", request.getEntity());
				sb.append("&").append("entity={entity}");
			}

			if (StringUtils.isNotBlank(String.valueOf(request.getLimit()))) {
				map.put("limit", request.getLimit());
				sb.append("&").append("limit={limit}");
			}


			response = restTemplate.getForObject(sb.toString(), ASResponse.class, map);
			response.setTerm(request.getTerm());
		} catch (Exception e) {
			log.error("Error", e);
		}

		return response;
	}

	public ASResponse get(String term, String media) {

		log.debug("AppStore get: {}", term);

		ASResponse response = null;
		try {
			response = restTemplate.getForObject(SEARCH_URL, ASResponse.class, term, media);
			response.setTerm(term);
		} catch (Exception e) {
			log.error("Error", e);
		}

		return response;
	}

	public ASResponse lookup(String id) {

		log.debug("AppStore lookup: {}", id);

		ASResponse response = null;
		try {
			response = restTemplate.getForObject(LOOKUP_URL, ASResponse.class, id);
		} catch (Exception e) {
			log.error("Error", e);
		}

		return response;
	}
}
