package pl.tom.appstore.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.tom.appstore.command.TermRanking;
import pl.tom.appstore.model.App;
import pl.tom.appstore.model.AppTerm;

@Component
public class RankService {

	public static final Logger log = LoggerFactory.getLogger(RankService.class);

	@Autowired
	ASClient asClient;

	public TermRanking getTermRanking(String term, App app) {
		ASResponse asResponse = asClient.get(term);

		TermRanking termRanking = new TermRanking();
		termRanking.setKeywords(term);

		if (asResponse == null) {
			return termRanking;
		}

		List<ASApp> asApps = asResponse.getResults();

		for (ASApp asApp : asApps) {
			if (asApp.getTrackId().equalsIgnoreCase(app.getAppId())) {
				termRanking.setiPadRank(asApps.indexOf(asApp) + 1);
				termRanking.setiPhoneRank(asApps.indexOf(asApp) + 1);
			}
		}
		return termRanking;
	}

	public List<TermRanking> getTermRankingsForTerms(List<String> terms, App app) {
		List<TermRanking> termRanks = new ArrayList<TermRanking>();

		if (CollectionUtils.isNotEmpty(terms)) {
			for (String term : terms) {
				termRanks.add(getTermRanking(term, app));
			}
		}
		return termRanks;
	}

	public List<TermRanking> getTermRankingsForAppTerms(List<AppTerm> appTerms, App app) {
		List<TermRanking> termRanks = new ArrayList<TermRanking>();

		if (CollectionUtils.isNotEmpty(appTerms)) {
			for (AppTerm appTerm : appTerms) {
				log.info(appTerm.getTerm());
				termRanks.add(getTermRanking(appTerm.getTerm(), app));
			}
		}
		return termRanks;
	}
}
