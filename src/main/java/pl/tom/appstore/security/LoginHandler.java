package pl.tom.appstore.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;
import pl.tom.appstore.service.AppService;

public class LoginHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private final static Logger log = LoggerFactory.getLogger(LoginHandler.class);

	@Autowired
	AppService appService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Authentication authentication) throws IOException, ServletException {
		User user = (User) authentication.getPrincipal();

		Collection<App> userApps = appService.getUserApps(user);

		log.info("User Apps {}", userApps);

		HttpSession session = httpServletRequest.getSession(true);
		session.setAttribute("userApps", userApps);

		super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
	}

}
