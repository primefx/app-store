package pl.tom.appstore.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import pl.tom.appstore.AjaxUtils;
import pl.tom.appstore.SessionHelper;
import pl.tom.appstore.UserUtils;
import pl.tom.appstore.api.ASApp;
import pl.tom.appstore.api.ASResponse;
import pl.tom.appstore.api.ASClient;
import pl.tom.appstore.command.AjaxResponse;
import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;
import pl.tom.appstore.service.AppService;

@SessionAttributes("userApps")
@Controller
@RequestMapping("/add-app")
public class AddAppController {

	private final static Logger log = LoggerFactory.getLogger(AddAppController.class);

	@Autowired
	ASClient appStoreClient;

	@Autowired
	AppService appService;

	@Autowired
	SessionHelper sessionHelper;

	@RequestMapping(method = RequestMethod.GET)
	public String view() {
		return "pop-up.add-app";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	AjaxResponse addApp(@RequestParam String newAppId, HttpServletResponse httpServletResponse, Model model,
			HttpSession session) {

		AjaxResponse ajaxResponse = null;

		ASResponse response = appStoreClient.lookup(newAppId);
		ASApp asApp = null;
		List<ASApp> asApps = response.getResults();
		if (!asApps.isEmpty()) {
			asApp = asApps.get(0);
		}
		try {
			if (asApp != null) {

				User user = UserUtils.getUser();

				App app = appService.findByAppIdAndUser(newAppId, user);

				if (app != null) {
					return AjaxUtils.errorResponse("App already exists");
				}

				app = appService.addApp(asApp, user);

				// Refresh user apps
				model.addAttribute("userApps", appService.getUserApps(user));

				sessionHelper.setCurrentApp(app, session);

				ajaxResponse = AjaxUtils.okResponse();
			} else {
				ajaxResponse = AjaxUtils.errorResponse("App Not Found");
			}
		} catch (Exception e) {
			log.error("AddApp", e);
			ajaxResponse = AjaxUtils.errorResponse("Error");
			httpServletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return ajaxResponse;
	}
}
