package pl.tom.appstore;

import org.springframework.security.core.context.SecurityContextHolder;

import pl.tom.appstore.model.User;

public class UserUtils {

	public static User getUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
