package pl.tom.appstore;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import pl.tom.appstore.model.Role;
import pl.tom.appstore.model.User;
import pl.tom.appstore.service.RoleService;
import pl.tom.appstore.service.UserService;

public class StartupAction {

	@Autowired
	UserService userService;

	@Autowired
	RoleService roleService;

	@PostConstruct
	public void init() {
		Set<Role> roles = new HashSet<Role>();

		Role role = new Role();
		role.setName("ROLE_ADMIN");
		roleService.add(role);
		roles.add(role);

		role = new Role();
		role.setName("ROLE_USER");
		roleService.add(role);
		roles.add(role);

		User user = new User();
		user.setRoles(roles);
		user.setEnabled(true);
		user.setPassword("admin");
		user.setUsername("admin");
		userService.add(user);
	}
}
