package pl.tom.appstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.tom.appstore.model.Role;
import pl.tom.appstore.repository.RoleRepository;

@Service
public class RoleService {

	@Autowired
	RoleRepository roleRepository;

	public Role add(Role role) {
		return roleRepository.save(role);
	}

}
