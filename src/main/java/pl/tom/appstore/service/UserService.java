package pl.tom.appstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.tom.appstore.model.User;
import pl.tom.appstore.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User add(User user) {
		return userRepository.save(user);
	}

	public User get(long id) {
		return userRepository.findOne(id);
	}

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
