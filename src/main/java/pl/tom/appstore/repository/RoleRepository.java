package pl.tom.appstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.tom.appstore.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
