<%@ include file="/WEB-INF/includes/taglibs.jsp" %>
<style>
.modal-body {
	overflow: visible;
}

.modal-footer {
	position: relative;
	z-index: 1;
}
</style>
<form:form id="addForm" action="/add-app" method="POST" >
	<input id="autocomplete" type="text" data-provide="typeahead" autocomplete="off" >
	<input id="newAppId" type="hidden" />
</form:form>
<script type="text/javascript">

	$(document).ready(function() {
	    
	    function addApp(){
			$.ajax({
				url : '<c:url value="/add-app" />',
				type: 'POST',
				dataType : 'json',
				data : {
				    newAppId : $('#newAppId').val()
				},
				success : function(data) {
				    if (!data.error) {
				    	window.location.reload();
				    } else {
						$.bootstrapGrowl(data.message);
				    }
				},
				error: function(){
				    $.bootstrapGrowl('Error');
				    console.log('Error');
				}
			});
	    }
	    
	    
	    $('#chooseApp .btn-primary').on('click', function(){
			addApp();
	    });
	    
	    $('#addForm').on('submit', function(){
			addApp();
			return false;
	    });
	    
		var labels, mapped = {};
		var newAppId = $('#newAppId');	

		 $('#autocomplete').typeahead({
			source : function(query, process) {
				console.log('go');
				$.ajax({
					url : '<c:url value="/as-api/search" />',
					dataType : 'json',
					type: 'GET',
					data : {
						term : query,
						media : 'software'
					},
					success : function(data) {
						console.log('query');
						labels = [];
						mapped = {};
						$.each(data.results, function(i, app) {
							mapped[app.trackName] = app;
							labels.push(app.trackName);
						});

						process(labels);
					},
					error : function(){
					    $.bootstrapGrowl('Error');
					    console.log('Error');
					}
				});
			},

			updater : function(item) {
			    newAppId.val(mapped[item].trackId);
				return item;
			}
		}); 
	});
</script>