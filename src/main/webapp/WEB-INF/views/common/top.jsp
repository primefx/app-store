<%@ include file="/WEB-INF/includes/taglibs.jsp"%>


<div class="row-fluid">
	<sec:authorize access="isAuthenticated()">
		
		<div class="span2">
			<a href="<c:url value='/add-app' />" data-target="#chooseApp" class="btn btn-primary" data-toggle="modal"><i class="icon-plus icon-white"></i> <spring:message code="button.add.new.app" /></a>

			<div class="modal hide" id="chooseApp" tabindex="-1">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3>
						<spring:message code="choose.app" />
					</h3>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal">
						<spring:message code="button.close" />
					</button>
					<button class="btn btn-primary" >
						<spring:message code="button.choose" />
					</button>
				</div>
			</div>
		</div>


		<div class="span3">
			<form:form id="changeAppForm" action="/app/change-app" cssClass="form-inline">
				<select id="appId" name="appId">
					<c:forEach items="${userApps }" var="app">
						<option <c:if test="${app.appId == currentApp.appId }" >selected</c:if> value="${app.appId}">${ app.name}</option>
					</c:forEach>
				</select>
			</form:form>
		</div>
		<div class="span3">
			<form:form id="changeAppLangForm" action="/app/change-lang" cssClass="form-inline">
				<select id="appLang" name="appLang">
					<c:forTokens items="EN,PL,US,CH,FR,JP" delims="," var="item">
						<option <c:if test="${item eq currentAppLang }" >selected</c:if> value="${item}">${ item}</option>
					</c:forTokens>
				</select>
			</form:form>
		</div>
	</sec:authorize>

	<div class="span1 offset2">
		<sec:authorize access="isAnonymous()">
			<a href="<c:url value='login' />">Log in</a>
		</sec:authorize>
		<sec:authorize access="isAuthenticated()">
			<sec:authentication property="principal.username" />
			<a href="<c:url value="/logout" />"><i class="icon-off"></i></a>
		</sec:authorize>
	</div>
</div>

<ul class="nav nav-tabs">
	<li><a href="<c:url value='search-ranking' />">Search Ranking</a></li>
	<li><a href="#">Tracked Apps</a></li>
	<li><a href="#">Keyword Research</a></li>
	<li><a href="#">Keyword Spy</a></li>
</ul>

<script>
    $(document).ready(function() {
	$('#appId').on('change', function(event) {
	    $.ajax({
			url : '<c:url value="/app/change-app" />',
			type : 'POST',
			dataType : 'json',
			data : {
			    appId : $('#appId').val()
			},
			success : function(data) {
			    if (!data.error) {
			    	window.location.reload();
			    } else {
					$.bootstrapGrowl(data.message, {
					    type: 'error'
					});
			    }
			},
			error : function(e) {
			    $.bootstrapGrowl('Error');
			    console.log('Error');
			}
	    });
	});

	$('#appLang').on('change', function(event) {
	    $.ajax({
			url : '<c:url value="/app/change-lang" />',
			type : 'POST',
			dataType : 'json',
			data : {
			    appLang : $('#appLang').val()
			},
			success : function(data) {
			    if (!data.error) {
			    	window.location.reload();
			    } else {
					$.bootstrapGrowl(data.message, {
					    type: 'error'
					});
			    }
			},
			error : function(e) {
			    $.bootstrapGrowl('Error');
			    console.log('Error');
			}
	    });
	});
    });
</script>