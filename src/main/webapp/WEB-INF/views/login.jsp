<%@ include file="/WEB-INF/includes/taglibs.jsp"%>


<c:if test="${not empty param.error}">
	<div>
		Login error.<br /> Reason :
		${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
	</div>
</c:if>

<form action="<c:url value='processLogin' />" method="POST" class="form-horizontal" >

	<div class="control-group">
		<label class="control-label" for="userName">User</label>
		<div class="controls">	
			<input id="userName" type="text" name="userName" placeholder="Email"/>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="password">Password</label>		
		<div class="controls">
			<input id="password" type="password" name="password" placeholder="Password" />
		</div>
	</div>
	
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">Login</button>
		</div>	
	</div>
	
</form>