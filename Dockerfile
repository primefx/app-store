FROM java:8

ADD target/appstore.war /data/appstore.war

EXPOSE 8080

CMD java -jar /data/appstore.war